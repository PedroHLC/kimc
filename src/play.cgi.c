/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#include <kimc/game.h>
#include <kimc/utils.h>
#include <kimc/cgi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>

const static char UINT_F[]="%u";

int main() {
	kc_fixpath();
	srand(time(NULL));

	char *parameters = getenv("QUERY_STRING");
	if(parameters == NULL)
		return kccgi_diebyparams();

	KCPlayer player;
	if(sscanf(parameters, "sess=%u+%u+%u", &player.game, &player.id, &player.password) != 3)
		return kccgi_diebyparams();
	
	if(!kcplayer_validate(&player))
		return kccgi_diebyinvalidpass();

	kcplayer_read(&player);
	KCGame game;
	kcgame_read(player.game, &game);

	KCRoleData *role = kcroles_data_get(game.roles[player.id]);

	char game_s[64], slot_s[64], psw_s[64];
	sprintf(game_s, UINT_F, player.game);
	sprintf(slot_s, UINT_F, player.id);
	sprintf(psw_s, UINT_F, player.password);
	
	KCPageBegin();
	KCPageTitle("O Jogo");
	kc_cat(KCF_PgSnippets_BootstrapHeader, stdout);
	KCPageGoBody();
	kc_fcat(KCF_PgSnippets_GameBase, stdout, player.nick, role->name, role->pros, role->cons, role->obj, game_s, game_s, slot_s, psw_s);

	KCPageEnd();
	
	return 0;
}