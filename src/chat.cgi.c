/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#include <kimc/game.h>
#include <kimc/utils.h>
#include <kimc/cgi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>

const char _KCHTM_H0[] = "Content-Type: text/html; charset=UTF-8\r\n\r\n";

int main() {
	kc_fixpath();
	srand(time(NULL));

	char *parameters = getenv("QUERY_STRING");
	if(parameters == NULL)
		return kccgi_diebyparams();

	long int off, end;
	KCGameId game_id;
	if(sscanf(parameters, "game=%u&off=%ld&end=%ld", &game_id, &off, &end) != 3)
		return kccgi_diebyparams();
	
	fwrite(_KCHTM_H0,sizeof(_KCHTM_H0)-1, 1, stdout);

	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Game, game_id);
	
	FILE *f;
	
	sprintf(fname, KCF_Game_Chat, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return 0;
	
	fseek(f, off, SEEK_SET);
	
	size_t datasz = end-off;
	void *data = malloc(datasz);
	fread(data, 1, datasz, f);

	fwrite(data, 1, datasz, stdout);

	fclose(f);
	
	return 0;
}