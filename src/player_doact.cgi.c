/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#include <kimc/game.h>
#include <kimc/utils.h>
#include <kimc/cgi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	kc_fixpath();
	
	char *parameters = getenv("QUERY_STRING");
	if(parameters == NULL)
		return kccgi_diebyparams();

	KCPlayer player = {0};
	unsigned int target=KC_PLAYERID_INVALID;

	if(sscanf(parameters, "sess=%u+%u+%u&target=%u", &player.game, &player.id, &player.password, &target) != 4)
		return kccgi_diebyparams();

	if(!kcplayer_validate(&player))
		return kccgi_diebyinvalidpass();
	
	kcplayer_read(&player);

	KCGame game;
	kcgame_read(player.game, &game);
	KCRoles player_role = game.roles[player.id];

	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Player, player.game, player.id);

	printf(CGI_PLAINTEXT "Status: 200 OK\r\n\r\n");
	if(player.state == ST_ALIVE) {
		if(
			(game.state == DS_DAY || game.state == DS_NIGHT) && //DIA OU NOITE
			(game.state != DS_NIGHT || player_role != RL_DUMMYTOWNER) && //DUMMY NAO VISITA
			(target != player.id || player_role == RL_MEDIC) && // DIFERENTE DE VC MESMO
			(game.state != DS_NIGHT || player_role != RL_MAFIOSO || game.roles[target] != RL_MAFIOSO) //MAFIOSOS NÃO SE MATAM A NOITE
		) {
			KCPlayer target_plr = {
				.id = target,
				.game = player.game
			};
			kcplayer_read(&target_plr);
			if(target_plr.state == ST_ALIVE) {
				sprintf(fname,
					(game.state == DS_DAY ? KCF_Player_Vote : KCF_Player_Night),
					fdir, game.day);
				FILE *f = fopen(fname, KC_F_W);
				KCPlayerId target_s = target;
				if(f) {
					fwrite(&target_s, sizeof(target_s), 1, f);
					fclose(f);
					if(game.state == DS_DAY) {
						FILE *chat = kcgame_chat_open(player.game);
						KCChatAnnounce(chat, "\"%s\" votou contra \"%s\".", player.nick, target_plr.nick);
						fclose(chat);
					}
				}
			}
		} else if(game.state == DS_JUDGMENT) {
			sprintf(fname, KCF_Player_Judgm, fdir, game.day);
			FILE *f = fopen(fname, KC_F_W);
			unsigned char target_s = target;
			if(f) {
				fwrite(&target_s, sizeof(target_s), 1, f);
				fclose(f);
			}
		}
		puts("OK");
		return 0;
	} else {
		puts("NOTALIVE");
		return 0;
	}
}