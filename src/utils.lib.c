/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#include <kimc/game.h>
#include <kimc/utils.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/sendfile.h>

const char *KC_F_W = "wb", *KC_F_R = "rb", *KC_F_A = "ab";

#define read_opts(in, out, fname, fdir, f, fsz) \
	sprintf(fname, in, fdir); \
	f = fopen(fname, KC_F_R); \
	if(f) { \
		fsz = ftell(f); \
		rewind(f); \
		out = (char *) malloc(fsz+1); \
		out[fsz] = 0; \
		fread(out, sizeof(char), fsz, f); \
		fclose(f); \
	} else \
		out = NULL; \

void mkdir_p(char *dir) {
	char tmp[KC_FPATH_LIMIT];
	char *p = NULL;
	size_t len;

	snprintf(tmp, sizeof(tmp),"%s",dir);
	len = strlen(tmp);
	if(tmp[len - 1] == '/')
		tmp[len - 1] = 0;
	for(p = tmp + 1; *p; p++)
		if(*p == '/') {
			*p = 0;
			mkdir(tmp, S_IRWXU);
			*p = '/';
		}
	mkdir(tmp, S_IRWXU);
}

void urldecode2(char *dst, const char *src) {
	// TAKEN FROM: http://stackoverflow.com/posts/14530993
	char a, b;
	while (*src) {
		if ((*src == '%') &&
		    ((a = src[1]) && (b = src[2])) &&
		    (isxdigit(a) && isxdigit(b))) {
			if (a >= 'a')
				a -= 'a'-'A';
			if (a >= 'A')
				a -= ('A' - 10);
			else
				a -= '0';
			if (b >= 'a')
				b -= 'a'-'A';
			if (b >= 'A')
				b -= ('A' - 10);
			else
				b -= '0';
			*dst++ = 16*a+b;
			src+=3;
		} else if (*src == '+') {
			*dst++ = ' ';
			src++;
		} else {
			*dst++ = *src++;
		}
	}
	*dst++ = '\0';
}

void kc_fixpath() {
	char fname[KC_FPATH_LIMIT], *run_path, def_run_path[]=RUN_PATH;
	run_path = getenv("RUN_PATH");
	if(!run_path)
		run_path = def_run_path;

	if(run_path[0] == '/') {
		//puts(run_path);
		chdir(run_path);
	}

	else {
		readlink("/proc/self/exe", fname, KC_FPATH_LIMIT-1);
		char *lp = strrchr(fname, '/');
		if(lp != NULL)
			*lp = 0;
		strcat(fname, "/");
		strcat(fname, run_path);
		//puts(fname);
		chdir(fname);
	}
}

KCGame *kcgame_read(KCGameId id, KCGame *dest) {
	dest->id = id;

	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Game, dest->id);
	
	FILE *f;
	
	sprintf(fname, KCF_Game_PlayersNum, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return NULL;
	fread(&dest->p_num, sizeof(dest->p_num), 1, f);
	fclose(f);
	
	sprintf(fname, KCF_Game_Day, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return NULL;
	fread(&dest->day, sizeof(dest->day), 1, f);
	fclose(f);
	
	sprintf(fname, KCF_Game_State, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return NULL;
	fread(&dest->state, sizeof(dest->state), 1, f);
	fclose(f);

	sprintf(fname, KCF_Game_Timer, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return NULL;
	fread(&dest->timer, sizeof(dest->timer), 1, f);
	fclose(f);

	sprintf(fname, KCF_Game_Roles, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return false;
	fread(&dest->slots_max, sizeof(dest->slots_max), 1, f);
	dest->roles = (KCRoles*) calloc(sizeof(KCRoles), dest->slots_max);
	fread(dest->roles, sizeof(KCRoles), dest->slots_max, f);
	fclose(f);
	
	sprintf(fname, KCF_Game_Judging, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return NULL;
	fread(&dest->judging, sizeof(dest->judging), 1, f);
	fclose(f);

	return dest;
}

bool kcgame_write(KCGame *game) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Game, game->id);
	mkdir_p(fdir);
	
	FILE *f;
	
	sprintf(fname, KCF_Game_PlayersNum, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&game->p_num, sizeof(game->p_num), 1, f);
	fclose(f);
	
	sprintf(fname, KCF_Game_Day, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&game->day, sizeof(game->day), 1, f);
	fclose(f);
	
	sprintf(fname, KCF_Game_State, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&game->state, sizeof(game->state), 1, f);
	fclose(f);

	sprintf(fname, KCF_Game_Timer, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&game->timer, sizeof(game->timer), 1, f);
	fclose(f);

	sprintf(fname, KCF_Game_Roles, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&game->slots_max, sizeof(game->slots_max), 1, f);
	fwrite(game->roles, sizeof(KCRoles), game->slots_max, f);
	fclose(f);

	sprintf(fname, KCF_Game_Judging, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&game->judging, sizeof(game->judging), 1, f);
	fclose(f);

	return true;
}

KCPlayerId kcgame_slots_pop(KCGame *game) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Game, game->id);
	mkdir_p(fdir);
	sprintf(fname, KCF_Game_PlayersNum, fdir);

	FILE *f;
	KCPlayerId p_num = 0, send = 0;
	
	f = fopen(fname, KC_F_R);
	if(!f)
		return 0;
	fread(&p_num, sizeof(p_num), 1, f);
	fclose(f);

	p_num++;
	if(p_num > game->slots_max)
		return KC_PLAYERID_INVALID;
	
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&p_num, sizeof(p_num), 1, f);
	fclose(f);

	return p_num-1;
}

KCPlayer *kcgame_players_load(KCGame *game) {
	size_t players_num = game->p_num, plr_wrki;
	KCPlayer *dest = NULL, *plr_wrk, *plr_f;
	dest = (KCPlayer *) malloc(sizeof(*dest) * players_num);
	if(!dest)
		return NULL;
	plr_f = &dest[players_num];
	plr_wrki = 0;
	for(plr_wrk = dest; plr_wrk < plr_f;) {
		*plr_wrk = (KCPlayer){
			.game = game->id,
			.id = plr_wrki
		};
		kcplayer_read(plr_wrk);
		plr_wrki++;
		plr_wrk++;
	}

	return dest;
}

long int kcgame_chat_size(KCGameId game_id) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Game, game_id);
	
	FILE *f;
	
	sprintf(fname, KCF_Game_Chat, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return 0;
	long int fsz;
	/*fseek(f, 0L, SEEK_END);
	fsz = ftell(f);*/
	struct stat fs;
	fstat(fileno(f), &fs);
	fsz = fs.st_size;
	fclose(f);

	return fsz;

}

FILE *kcgame_chat_open(KCGameId game_id) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Game, game_id);
	
	FILE *f;
	
	sprintf(fname, KCF_Game_Chat, fdir);
	f = fopen(fname, KC_F_A);
	if(!f)
		return 0;

	return f;
}

bool kcgame_state_update(KCGame *game) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Game, game->id);
	mkdir_p(fdir);
	
	FILE *f;
	
	sprintf(fname, KCF_Game_Day, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&game->day, sizeof(game->day), 1, f);
	fclose(f);
	
	sprintf(fname, KCF_Game_State, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&game->state, sizeof(game->state), 1, f);
	fclose(f);

	sprintf(fname, KCF_Game_Timer, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&game->timer, sizeof(game->timer), 1, f);
	fclose(f);

	sprintf(fname, KCF_Game_Judging, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&game->judging, sizeof(game->judging), 1, f);
	fclose(f);

	return true;
}


int vote_compare (const void * a, const void * b) {
  return ( *(unsigned int*)a - *(unsigned int*)b );
}

KCPlayerId kcgame_judging_get(KCGame *game, unsigned char day) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	KCPlayerId *targets=NULL;
	size_t targets_sz=0;
	KCPlayerId actpid;
	for(actpid = 0; actpid < game->slots_max; actpid++) {
		sprintf(fdir, KCD_Player, game->id, actpid);
		sprintf(fname, KCF_Player_Vote, fdir, day);
		FILE *f = fopen(fname, KC_F_R);
		if(f) {
			targets_sz++;
			targets = (KCPlayerId*)(targets ? realloc(targets, targets_sz * sizeof(*targets)) : malloc(sizeof(*targets)));
			fread(&targets[targets_sz-1], sizeof(*targets), 1, f);
			fclose(f);
		}
	}
	if(targets_sz < 1)
		return KC_PLAYERID_INVALID;
	else if(targets_sz == 1)
		return targets[0];
	qsort(targets, targets_sz, sizeof(*targets), vote_compare);

	KCPlayerId *last=targets, *h=targets, *s=targets, *act=&targets[1], *end=&targets[targets_sz];
	size_t act_count=1, high_counter=1;
	while(act < end) {
		if(*act == *last)
			act_count++;
		else {
			if(act_count > high_counter)  {
				h = s;
				high_counter = act_count;
			}
			s = act;
			act_count = 1;
		}
		act++;
	}
	if(act_count > high_counter)  {
		h = s;
		high_counter = act_count;
	}

	return *h;
}

KCJudgResult kcgame_judging_isguilty(KCGame *game, unsigned char day) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	unsigned char glt=0,innc=0, z;
	KCPlayerId actpid;
	for(actpid = 0; actpid < game->slots_max; actpid++) {
		sprintf(fdir, KCD_Player, game->id, actpid);
		sprintf(fname, KCF_Player_Judgm, fdir, day);
		FILE *f = fopen(fname, KC_F_R);
		if(f) {
			fread(&z, sizeof(z), 1, f);
			fclose(f);
			if(z == VT_INNOCENT)
				innc++;
			else if(z == VT_GUILTY)
				glt++;
		}
	}
	if(glt == innc)
		return VT_DRAW;
	return(glt > innc ? VT_GUILTY : VT_INNOCENT);
}

void kcgame_killplayer(KCGameId game_id, KCPlayerId player_id) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Player, game_id, player_id);
	sprintf(fname, KCF_Player_State, fdir);
	KCPlayerState state=ST_DEAD;
	FILE *f = fopen(fname, KC_F_R);
	if(f) {
		fread(&state, sizeof(state), 1, f);
		fclose(f);
	}
	if(state == ST_ALIVE) {
		state = ST_DEAD;
		if(f = fopen(fname, KC_F_W)) {
			fwrite(&state, sizeof(state), 1, f);
			fclose(f);
		}
	}
}

KCPlayerId kcplayer_action_target(KCPlayer *player, unsigned char day) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Player, player->game, player->id);
	sprintf(fname, KCF_Player_Night, fdir, day);
	KCPlayerId resp=KC_PLAYERID_INVALID;
	FILE *f = fopen(fname, KC_F_R);
	if(f) {
		fread(&resp, sizeof(resp), 1, f);
		fclose(f);
	}
	return resp;
}

bool kcplayer_write(KCPlayer *player) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Player, player->game, player->id);
	mkdir_p(fdir);
	
	FILE *f;
	
	sprintf(fname, KCF_Player_Password, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&player->password, sizeof(player->password), 1, f);
	fclose(f);
	
	sprintf(fname, KCF_Player_Nick, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fputs(player->nick, f);
	fclose(f);
	
	sprintf(fname, KCF_Player_State, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&player->state, sizeof(player->state), 1, f);
	fclose(f);

	sprintf(fname, KCF_Player_LastWill, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	if(player->last_will)
		fputs(player->last_will, f);
	fclose(f);

	sprintf(fname, KCF_Player_DeathNote, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	if(player->death_note)
		fputs(player->death_note, f);
	fclose(f);

	sprintf(fname, KCF_Player_Killers, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&player->killers_n, sizeof(player->killers_n), 1, f);
	if(player->killers_n > 0 && player->killers)
		fwrite(player->killers, sizeof(KCPlayerId), player->killers_n, f);
	fclose(f);

	return true;
}

bool kcplayer_nick_write(KCPlayer *player) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Player, player->game, player->id);
	mkdir_p(fdir);
	
	FILE *f;
	
	sprintf(fname, KCF_Player_Nick, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fputs(player->nick, f);
	fclose(f);

	return true;
}

bool kcplayer_validate(KCPlayer *player) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Player, player->game, player->id);
	
	unsigned int password;

	sprintf(fname, KCF_Player_Password, fdir);
	FILE *f = fopen(fname, KC_F_R);
	if(!f)
		return NULL;
	fread(&password, sizeof(password), 1, f);
	fclose(f);

	return(player->password == password);
}

KCPlayer *kcplayer_read(KCPlayer *player) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Player, player->game, player->id);
	FILE *f;

	sprintf(fname, KCF_Player_Nick, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return NULL;
	fgets(player->nick, 20, f);
	fclose(f);

	sprintf(fname, KCF_Player_State, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return NULL;
	fread(&player->state, sizeof(player->state), 1, f);
	fclose(f);

	long int fsz;
	read_opts(KCF_Player_LastWill, player->last_will, fname, fdir, f, fsz);
	read_opts(KCF_Player_DeathNote, player->death_note, fname, fdir, f, fsz);
	read_opts(KCF_Player_DeathNote, player->death_note, fname, fdir, f, fsz);

	sprintf(fname, KCF_Player_Killers, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return NULL;
	fread(&player->killers_n, sizeof(player->killers_n), 1, f);
	if(player->killers_n > 0)
		fread(player->killers, sizeof(KCPlayerId), player->killers_n, f);
	fclose(f);

	return player;
}

bool kcplayer_state_update(KCPlayer *player) {
	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Player, player->game, player->id);
	mkdir_p(fdir);
	
	FILE *f;
	
	sprintf(fname, KCF_Player_State, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&player->state, sizeof(player->state), 1, f);
	fclose(f);

	sprintf(fname, KCF_Player_Killers, fdir);
	f = fopen(fname, KC_F_W);
	if(!f)
		return false;
	fwrite(&player->killers_n, sizeof(player->killers_n), 1, f);
	if(player->killers_n > 0 && player->killers)
		fwrite(player->killers, sizeof(KCPlayerId), player->killers_n, f);
	fclose(f);

	return true;
}

KCRoleData *KC_ROLES_CACHE[_RL_NUM] = {NULL};
KCRoleData *kcroles_data_get(KCRoles id) {
	if(KC_ROLES_CACHE[id-1])
		return KC_ROLES_CACHE[id-1];

	char fname[KC_FPATH_LIMIT], fdir[KC_FPATH_LIMIT];
	sprintf(fdir, KCD_Role, id);
	FILE *f;
	
	sprintf(fname, KCF_Role_Name, fdir);
	f = fopen(fname, KC_F_R);
	if(!f)
		return NULL;
	KCRoleData *p = (KCRoleData *) malloc(sizeof(*p));
	p->id = id;
	fgets(p->name, 20, f);
	fclose(f);

	long int fsz;
	read_opts(KCF_Role_Desc, p->desc, fname, fdir, f, fsz);
	read_opts(KCF_Role_Pros, p->pros, fname, fdir, f, fsz);
	read_opts(KCF_Role_Cons, p->cons, fname, fdir, f, fsz);
	read_opts(KCF_Role_Obj, p->obj, fname, fdir, f, fsz);

	return(KC_ROLES_CACHE[id-1] = p);
}

void kcroles_rand(size_t rn, KCRoles *r) {
	KCRoles *ri, *rr, *rf=(r + rn), raux;
	for(ri=r; ri < rf; ri++) {
		rr = ri+(rand() % rn);
		rn--;
		if(rr != ri) {
			raux = *ri;
			*ri = *rr;
			*rr = raux;
		}
	}
}

void kc_cat(char *fname, FILE *out) {
	fflush(out);
	int f = open(fname, O_RDONLY);
	struct stat fileinfo = {0};
	fstat(f, &fileinfo);
	sendfile(fileno(out), f, NULL, fileinfo.st_size);
	//splice(f, NULL, fileno(out), 0);
	close(f);
}

bool kc_fcat(char *fname, FILE *stream, ...) {
	va_list vals;
	va_start(vals, stream);
	FILE *f = fopen(fname, KC_F_R);
	if(!f)
		return false;
	char k[4096], *l, *m, *p;
	k[4095] = 0;
	while(fgets(k, sizeof(k)-1, f) != NULL) {
		m = l = k;
		while((m = strchr(l, 1)) != 0) {
			*m = 0;
			fputs(l, stream);
			p = va_arg(vals, char*);
			if(p)
				fputs(p, stream);
			l = m+1;
		}
		fputs(l, stream);
	}
	fclose(f);
	va_end(vals);
	return true;
}