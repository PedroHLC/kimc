/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#include <kimc/game.h>
#include <kimc/utils.h>
#include <kimc/cgi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	kc_fixpath();
	
	char *parameters = getenv("QUERY_STRING");
	if(parameters == NULL)
		return kccgi_diebyparams();

	KCPlayer player = {0};
	char msg_esc[256], *msg;

	if(sscanf(parameters, "sess=%u+%u+%u&msg=%s", &player.game, &player.id, &player.password, &msg_esc) != 4)
		return kccgi_diebyparams();
	
	if(!kcplayer_validate(&player))
		return kccgi_diebyinvalidpass();
	
	msg = (char*) malloc(strlen(msg_esc)+1);
	urldecode2(msg, msg_esc);

	kcplayer_read(&player);

	printf(CGI_PLAINTEXT "Status: 200 OK\r\n\r\n");
	if(player.state == ST_ALIVE) {
		FILE *chat = kcgame_chat_open(player.game);
		if(!chat) {
			puts("FAIL");
			return 0;
		}
		kcxml_tag_open(HTML_SPAN, 1, (KCXMLParam[]){
			(KCXMLParam){HTML__CLASS, CHAT_CLASS_PLAYER}
		}, chat);
		fprintf(chat, "[%s]: %s", player.nick, msg);
		kcxml_tag_close(HTML_SPAN, chat);
		kcxml_tag_ins(HTML_BR, 0, NULL, chat);
		fclose(chat);
		puts("OK");
	} else {
		puts("NOTALIVE");
	}

	free(msg);
	return 0;
}