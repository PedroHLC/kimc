/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#include <kimc/game.h>
#include <kimc/utils.h>
#include <kimc/cgi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>

const static char UINT_F[]="%u";

int main() {
	kc_fixpath();
	srand(time(NULL));

	char *parameters = getenv("QUERY_STRING");
	if(parameters == NULL)
		return kccgi_diebyparams();

	KCPlayer player;
	char nick_esc[60], nick[60];
	if((sscanf(parameters, "game=%u&slot=%u&psw=%u&nick=%s", &player.game, &player.id, &player.password, nick_esc) == 4)) {
		if(!kcplayer_validate(&player))
			return kccgi_diebyinvalidpass();
		kcplayer_read(&player);
		
		urldecode2(nick, nick_esc);
		nick[20] = 0;
		strcpy(player.nick, nick);
		kcplayer_nick_write(&player);

		if(player.state == ST_CONNECTING) {
			FILE *chat = kcgame_chat_open(player.game);
			KCChatAnnounce(chat, "\"%s\" entrou no jogo.", player.nick);
			fclose(chat);
			player.state = ST_ALIVE;
			kcplayer_state_update(&player);
		}

		KCPageBegin();
		KCPageTitle("Finalizando preparativos...");
		kc_cat(KCF_PgSnippets_BootstrapHeader, stdout);
		KCPageGoBody();

		kcxml_tag_open(HTML_SCRIPT, 0, NULL, stdout);
		printf("window.location.href=\"play.cgi?sess=%u+%u+%u\";",
			player.game, player.id, player.password);
		kcxml_tag_close(HTML_SCRIPT, stdout);

		KCPageEnd();
	} else if(sscanf(parameters, "game=%u&slot=%u&psw=%u", &player.game, &player.id, &player.password) != 3) {
		return kccgi_diebyparams();
	} else {
		if(!kcplayer_validate(&player))
			return kccgi_diebyinvalidpass();
		//TODO: Validar se conectando...

		char game_s[64], slot_s[64], psw_s[64];
		sprintf(game_s, UINT_F, player.game);
		sprintf(slot_s, UINT_F, player.id);
		sprintf(psw_s, UINT_F, player.password);

		char randname[10], *ri;
		*randname = 'A'+(rand()%('Z'-'A'));
		for(ri=randname+1; ri < randname+4+(rand()%5)-1; ri++)
			*ri = 'a'+(rand()%('z'-'a'));
		*ri = 0;

		KCPageBegin();
		KCPageTitle("Entrar nick...");
		kc_cat(KCF_PgSnippets_BootstrapHeader, stdout);
		KCPageGoBody();
		kc_fcat(KCF_PgSnippets_EnterNick, stdout, game_s, slot_s, psw_s, randname, randname);

		KCPageEnd();
	}
	
	return 0;
}