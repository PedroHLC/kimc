/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#include <kimc/cgi.h>
#include <stdio.h>
#include <string.h>

const char *XML_HTML = "html",
	*XML_HTML_LANG = "lang",
	*HTML_HEAD = "head",
	*HTML_META = "meta",
	*HTML_TITLE = "title",
	*HTML_LINK = "link",
	*HTML_LINK_REL = "rel",
	*HTML_LINK_TYPE = "type",
	*HTML_LINK_HREF = "href",
	*HTML_SCRIPT = "script",
	*HTML_BODY = "body",
	*HTML_DIV = "div",
	*HTML_SPAN = "span",
	*HTML_BR = "br",
	*HTML__CLASS = "class";

const char *LANG_PTBR="pt-br",
	*CHAT_CLASS_SERVER="server_announce",
	*CHAT_CLASS_PLAYER="player_speak";

const char *HTML_TAG = "</>",
	*HTML_TAGPARAM = " =\"";

void kcxml_tparam_ins(size_t params_num, KCXMLParam *params, FILE *stream) {
	while(params_num > 0) {
		fwrite(HTML_TAGPARAM, sizeof(char), 1, stream);
		fwrite(params->key, sizeof(char), strlen(params->key), stream);
		fwrite(&HTML_TAGPARAM[1], sizeof(char), 2, stream);
		fwrite(params->val, sizeof(char), strlen(params->val), stream);
		fwrite(&HTML_TAGPARAM[2], sizeof(char), 1, stream);
		params++;
		params_num--;
	}
}

void kcxml_tag_open(char *tag, size_t params_num, KCXMLParam *params, FILE *stream) {
	fwrite(HTML_TAG, sizeof(char), 1, stream);
	fwrite(tag, sizeof(char), strlen(tag), stream);
	kcxml_tparam_ins(params_num, params, stream);
	fwrite(&HTML_TAG[2], sizeof(char), 1, stream);
}

void kcxml_tag_close(char *tag, FILE *stream) {
	fwrite(HTML_TAG, sizeof(char), 2, stream);
	fwrite(tag, sizeof(char), strlen(tag), stream);
	fwrite(&HTML_TAG[2], sizeof(char), 1, stream);
}

void kcxml_tag_ins(char *tag, size_t params_num, KCXMLParam *params, FILE *stream) {
	fwrite(HTML_TAG, sizeof(char), 1, stream);
	fwrite(tag, sizeof(char), strlen(tag), stream);
	kcxml_tparam_ins(params_num, params, stream);
	fwrite(&HTML_TAG[1], sizeof(char), 2, stream);
}

void kcxml_tag_insc(char *tag, size_t params_num, KCXMLParam *params, char *contents, FILE *stream) {
	kcxml_tag_open(tag, params_num, params, stream);
	fwrite(contents, sizeof(char), strlen(contents), stream);
	kcxml_tag_close(tag, stream);
}

void kcxml_css_ins(char *url, FILE *stream) {
	KCXMLParam p[] = {
		{HTML_LINK_REL, "stylesheet"},
		{HTML_LINK_TYPE, "text/css"},
		{HTML_LINK_HREF, url}
	};
	kcxml_tag_ins(HTML_LINK, 2, p, stream);
}

const static char _KCCGI_H0[] = "Content-Type: text/html; charset=UTF-8\r\n\r\n<!DOCTYPE html>";
const static char _KCCGI_400[] = CGI_PLAINTEXT "Status: 400 Bad Request\r\n\r\nDesculpe, os parametros enviados são inválidos!\r\n";
const static char _KCCGI_401[] = CGI_PLAINTEXT "Status: 401 Unauthorized\r\n\r\nDesculpe, mas suas credenciais estão inválidas!\r\n";

void kccgi_html_begin(char *page_name) {
	fwrite(_KCCGI_H0,sizeof(_KCCGI_H0)-1, 1, stdout);
}

int kccgi_diebyparams() {
	fwrite(_KCCGI_400,sizeof(_KCCGI_400)-1, 1, stdout);
	return 0;
}

int kccgi_diebyinvalidpass() {
	fwrite(_KCCGI_401,sizeof(_KCCGI_401)-1, 1, stdout);
	return 0;
}