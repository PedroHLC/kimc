/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#include <kimc/game.h>
#include <kimc/utils.h>
#include <kimc/cgi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>

char fdir[KC_FPATH_LIMIT];

/*KCGameMode TOWN_OF_SALEM = {
	15, (KCRoles[]){
		RL_SERIALKILLER,
		RL_JESTER,
		RL_SHERIFF,
		RL_INVESTIGATOR,
		RL_MEDIC,
		RL_MEDIUM,
		RL_LOOKOUT,
		RL_JAILOR,
		RL_FRAMER,
		RL_VETERAN,
		RL_EXECUTIONER,
		RL_VIGILANT,
		RL_MAFIOSO,
		RL_MAFIOSO,
		RL_MAFIOSO
	}
}, POLTH = {
	5, (KCRoles[]){
		RL_SERIALKILLER,
		RL_SHERIFF,
		RL_DUMMYTOWNER,
		RL_DUMMYTOWNER,
		RL_DUMMYTOWNER
	}
};*/

KCGameId create_id() {
	KCGameId gen = 0;
	
	while(gen < 1 || access(fdir, F_OK) != -1) {
		gen = rand() % KC_GAMEID_MAX;
		sprintf(fdir, KCD_Game, gen);
	}

	return gen;
}

const char failed_msg[] = "Failed to create game!";

int main() {
	kc_fixpath();
	srand(time(NULL));
	
	KCGame game = {create_id(), 0, 0, DS_WAITINGPLAYERS, 0,
		5, (KCRoles[]){
			RL_SERIALKILLER,
			RL_DUMMYTOWNER,
			RL_DUMMYTOWNER,
			RL_DUMMYTOWNER,
			RL_DUMMYTOWNER
		}, KC_PLAYERID_INVALID};
	kcroles_rand(game.slots_max, game.roles);
	
	KCPageBegin();
	KCPageTitle("Criando sala...");
	KCPageGoBody();

	if(!kcgame_write(&game))
		fwrite(failed_msg, sizeof(char), strlen(failed_msg), stdout);
	else {
		kcxml_tag_open(HTML_SCRIPT, 0, NULL, stdout);
		printf("window.location.href=\"enter_game.cgi?id=%u\";",
			game.id);
		kcxml_tag_close(HTML_SCRIPT, stdout);
	}

	KCPageEnd();

	return 0;
}