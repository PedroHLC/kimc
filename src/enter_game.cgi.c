/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#include <kimc/game.h>
#include <kimc/utils.h>
#include <kimc/cgi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>

const char failed_msg[] = "Failed to create player!";

int main() {
	kc_fixpath();
	srand(time(NULL));

	char *parameters = getenv("QUERY_STRING");
	if(parameters == NULL)
		return kccgi_diebyparams();

	KCGame game = {0};
	if(sscanf(parameters, "id=%u", &game.id) != 1)
		return kccgi_diebyparams();

	rand();
	unsigned int psw = ((unsigned)rand()) % UINT_MAX;

	kcgame_read(game.id, &game);
	KCPlayer player = {
		game.id, kcgame_slots_pop(&game), psw,
		"", ST_CONNECTING, NULL, NULL, 0, NULL
	};

	KCPageBegin();
	KCPageTitle("Criando jogador...");
	KCPageGoBody();

	if(player.id == KC_PLAYERID_INVALID || !kcplayer_write(&player))
		fwrite(failed_msg, sizeof(char), strlen(failed_msg), stdout);
	else {
		kcxml_tag_open(HTML_SCRIPT, 0, NULL, stdout);
		printf("window.location.href=\"enter_nick.cgi?game=%u&slot=%u&psw=%u\";",
			game.id, player.id, psw);
		kcxml_tag_close(HTML_SCRIPT, stdout);
	}

	KCPageEnd();
	
	return 0;
}