/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#include <kimc/game.h>
#include <kimc/utils.h>
#include <kimc/cgi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char _KCJSON_H0[] = "Content-Type: application/json\r\n\r\n";

int main() {
	kc_fixpath();
	
	char *parameters = getenv("QUERY_STRING");
	if(parameters == NULL)
		return kccgi_diebyparams();

	KCPlayerPass playerdat;
	if(sscanf(parameters, "sess=%u+%u+%u", &playerdat.game, &playerdat.id, &playerdat.password) != 3)
		return kccgi_diebyparams();
	
	KCGame game;
	KCPlayer *players=NULL, *player=NULL, *plr_wrk, *plr_bf;
	kcgame_read(playerdat.game, &game);
	KCRoles *roles = game.roles, myrole = roles[playerdat.id];
	players = kcgame_players_load(&game);
	FILE *chat;

	if(playerdat.id < game.p_num)
	player = &players[playerdat.id];
	player->password = playerdat.password;

	if(!kcplayer_validate(player))
		return kccgi_diebyinvalidpass();
	
	fwrite(_KCJSON_H0,sizeof(_KCJSON_H0)-1, 1, stdout);
	printf("{\"players\":[");

	size_t players_num = game.p_num, plr_wrki;
	plr_bf = &players[players_num-1];
	plr_wrki = 0;
	KCPlayerId *ki, *kf;
	for(plr_wrk = players; plr_wrk <= plr_bf;) {
		printf("{\"nick\":\"%s\",\"state\":%d,\"lw\":", plr_wrk->nick, plr_wrk->state);
		if(plr_wrk->last_will)
			printf("\"%s\"", plr_wrk->last_will);
		else
			printf("0");
		printf(",\"role\":");
		if(plr_wrk->state == ST_DEAD || plr_wrk->id == playerdat.id || (myrole == RL_MAFIOSO && roles[plr_wrk->id] == RL_MAFIOSO))
			printf("%d", roles[plr_wrk->id]);
		else
			printf("0");
		printf(",\"dn\":");
		if(plr_wrk->death_note)
			printf("\"%s\"", plr_wrk->death_note);
		else
			printf("0");
		printf(",\"kl\":[");
		kf=&plr_wrk->killers[plr_wrk->killers_n];
		for(ki=plr_wrk->killers; ki<kf; ki++)
			printf("%d", *ki);
		printf("]}");

		if(plr_wrk != plr_bf)
			printf(",");

		plr_wrki++;
		plr_wrk++;
	}

	if(game.state == DS_WAITINGPLAYERS) {
		if(game.p_num >= game.slots_max) {
			bool canstart = true;
			for(plr_wrk = players; plr_wrk <= plr_bf; plr_wrk++)
				if(plr_wrk->state != ST_ALIVE) {
					canstart = false;
					break;
				}
			if(canstart) {
				game.day = 0;
				game.state = DS_DAY;
				game.timer = time(NULL)+15;
				kcgame_state_update(&game);
				chat = kcgame_chat_open(player->game);
				KCChatAnnounce(chat, "Jogo iniciou! <span class=\"veico\">A</span>");
				fclose(chat);
			}
		}
	} else if(game.state == DS_ENDED_TOWNERS || game.state == DS_ENDED_SK) {
	} else if(game.timer < time(NULL)) {
		switch(game.state) {
			case DS_DAY:
				game.state = (
					(game.day > 0 && (game.judging = kcgame_judging_get(&game, game.day)) != KC_PLAYERID_INVALID) ?
					DS_JUDGMENT : DS_NIGHT
				);
				if(game.judging != KC_PLAYERID_INVALID) {
					chat = kcgame_chat_open(game.id);
					KCChatAnnounce(chat, "Votação contra \"%s\" iniciou.", players[game.judging].nick);
					fclose(chat);
				}
				break;
			case DS_JUDGMENT:
				chat = kcgame_chat_open(game.id);
				if(kcgame_judging_isguilty(&game, game.day) == VT_GUILTY) {
					kcgame_killplayer(game.id, game.judging);
					kcplayer_read(&players[game.judging]);
					KCChatAnnounce(chat, "\"%s\" morreu por julgamento.", players[game.judging].nick);
				} else {
					KCChatAnnounce(chat, "\"%s\" foi declarado inocente.", players[game.judging].nick);
				}
				fclose(chat);
				game.judging = 0;
				game.state = DS_NIGHT;
				break;
			case DS_NIGHT:
				game.state = DS_DAY;
				KCPlayerId target;
				for(plr_wrk = players; plr_wrk <= plr_bf; plr_wrk++) {
					target = kcplayer_action_target(plr_wrk, game.day);
					if(target == KC_PLAYERID_INVALID)
						continue;
					switch(roles[plr_wrk->id]) {
						case RL_SERIALKILLER:
							if(players[target].state == ST_ALIVE) {
								kcgame_killplayer(game.id, target);

								chat = kcgame_chat_open(game.id);
								KCChatAnnounce(chat, "\"%s\" morreu. Foi morto por um Serial Killer.", players[target].nick);
								fclose(chat);
							}
						break;
					}
				}
				game.day++;
				break;
		}

		size_t goods_alive=0, sks_alive=0;
		for(plr_wrk = players; plr_wrk <= plr_bf; plr_wrk++) {
			if(plr_wrk->state == ST_ALIVE) {
				switch(roles[plr_wrk->id]) {
					case RL_SERIALKILLER:
						sks_alive++;
						break;
					case RL_DUMMYTOWNER:
						goods_alive++;
				}
			}
		}

		if(goods_alive > 0 && sks_alive == 0) {
			game.state = DS_ENDED_TOWNERS;
			game.timer = 0;
			KCChatAnnounce(chat, "Fim de jogo, vitória dos cidadãos! <span class=\"veico\">W</span>");
		} else if(sks_alive > 0 && goods_alive <= 1) {
			game.state = DS_ENDED_SK;
			game.timer = 0;
			KCChatAnnounce(chat, "Fim de jogo, vitória do assassino! <span class=\"veico\">H</span>");
		} else
			game.timer = time(NULL)+15;

		kcgame_state_update(&game);
	}

	printf("],\"loaded_chat\":%ld,\"state\":%d,\"timer\":%d,\"judging\":%d,\"day\":%u}\n", kcgame_chat_size(game.id), game.state, game.timer, game.judging, game.day);

	free(players);
	
	return 0;
}