/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

const char
	*KCD_Game = "run/kimc/games/%u",
	*KCF_Game_PlayersNum = "%s/slots.bin",
	*KCF_Game_Day = "%s/day.bin",
	*KCF_Game_State = "%s/state.bin",
	*KCF_Game_Timer = "%s/timer.bin",
	*KCF_Game_Roles = "%s/roles.bin",
	*KCF_Game_Chat = "%s/chat.htm",
	*KCF_Game_Judging = "%s/judging.bin",
	*KCD_Player = "run/kimc/games/%u/players/%u",
	*KCF_Player_Password = "%s/psw.bin",
	*KCF_Player_State = "%s/state.bin",
	*KCF_Player_Nick = "%s/nick.txt",
	*KCF_Player_Killers = "%s/killers.bin",
	*KCF_Player_LastWill = "%s/last_will.txt",
	*KCF_Player_DeathNote = "%s/death_note.txt",
	*KCF_Player_Vote = "%s/day%d_vote.bin",
	*KCF_Player_Judgm = "%s/judgm%d_vote.bin",
	*KCF_Player_Night = "%s/night%d_action.bin",
	*KCD_Role = "share/kimc/roles/%u",
	*KCF_Role_Name = "%s/name.txt",
	*KCF_Role_Desc = "%s/desc.htm",
	*KCF_Role_Pros = "%s/pros.htm",
	*KCF_Role_Cons = "%s/cons.htm",
	*KCF_Role_Obj = "%s/obj.htm";

#define KCD_PgSnippets "share/kimc/html"

const char
	*KCF_PgSnippets_BootstrapHeader = KCD_PgSnippets "/bootstrap_header.htm",
	*KCF_PgSnippets_EnterNick = KCD_PgSnippets "/enter_nick.htm",
	*KCF_PgSnippets_GameBase = KCD_PgSnippets "/game_base.htm";