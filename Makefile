RUN_PATH?=\"/home/pedrohlc/Projects/UFSCar/CAP/kimc/salem\"
CFLAGS=-Wno-discarded-qualifiers -D RUN_PATH="${RUN_PATH}" -g -I./include -L./lib
LINK_LIBS=-lkimc_assets -lkimc_utils -lkimc_cgi

CC=gcc
BUNDLE_AR=ar rcs

LS_LIBS=assets utils cgi
LS_PAGES=create_game enter_game enter_nick play status chat chat_send player_doact
LS_STATICS=kimc/index.html

INSTALL_PATH?=${HOME}/public_html

all: libs pages

build/%.o: src/%.lib.c
	${CC} -o $@ -c $< ${CFLAGS}

lib/libkimc_%.a: build/%.o
	${BUNDLE_AR} $@ $<

libs: $(foreach f, $(LS_LIBS), lib/libkimc_${f}.a)

bin/%.cgi: src/%.cgi.c libs
	${CC} -o $@ $< ${LINK_LIBS} ${CFLAGS}

pages: $(foreach f, $(LS_PAGES), bin/${f}.cgi)

.PHONY: clean install

install:
	mkdir -p ${INSTALL_PATH}/{cgi-bin,assets,common}
	$(foreach f, $(LS_PAGES), cp bin/${f}.cgi ${INSTALL_PATH}/cgi-bin/;)
	cp -ar share/kimc/common ${INSTALL_PATH}/
	cp -ar share/kimc/assets ${INSTALL_PATH}/
	cp -ar statics/* ${INSTALL_PATH}/

clean:
	$(foreach f, $(LS_LIBS), rm lib/libkimc_${f}.a;)
	$(foreach f, $(LS_PAGES), rm bin/${f}.cgi;)