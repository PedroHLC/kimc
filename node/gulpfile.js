// Declares
var gulp, gutil, jshint, sass, uglify, postcss, autoprefixer, cssmin, rename, addsrc, sourcemaps, yargs;

// Time only time can...
console.time('Loading requirements');

// Requirements
gulp = require('gulp');
jshint = require('gulp-jshint');
sass = require('gulp-sass');
uglify = require('gulp-uglify');
postcss = require('gulp-postcss');
autoprefixer = require('autoprefixer');
csswring = require('csswring');
rename = require('gulp-rename');
addsrc = require('gulp-add-src');
sourcemaps = require('gulp-sourcemaps');
gutil = require('gulp-util');
yargs = require('yargs');

// ...bring back your love again
console.timeEnd('Loading requirements');

// Directory
var assets_folder = '../share/kimc/assets';

// Settings
var browser_support = ['> 1% in BR', 'Firefox >= 4', 'ie >= 6'];
var fonts_formats = ['ttf', 'eot', 'woff', 'woff2'];

// Error handler
// Command line option:  --fatal=[warning|error|off]
var fatalLevel = yargs.argv.fatal;
var ERROR_LEVELS = ['error', 'warning'];
function isFatal(level) {
   return ERROR_LEVELS.indexOf(level) <= ERROR_LEVELS.indexOf(fatalLevel || 'error');
}
function handleError(level, error) {
   gutil.log(error.message);
   if (isFatal(level)) {
      process.exit(1);
   }
}
function onError(error) { handleError.call(this, 'error', error);}
function onWarning(error) { handleError.call(this, 'warning', error);}

// Lint Task
gulp.task('lint', function() {
	return gulp.src(assets_folder+'/scripts.src/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

gulp.task('styles', function() {
	return gulp.src(assets_folder+'/styles.src/*.{scss,sass}')

	 	.on('error', onWarning)

		.pipe(sass({errLogToConsole: true}))
		.pipe(addsrc(assets_folder+'/styles.src/*.css'))

		.pipe(postcss([autoprefixer({ browsers: browser_support })]))
		.pipe(rename(function (path) { path.extname = ".css"; }))
		.pipe(gulp.dest(assets_folder+'/styles'))
		
		.pipe(sourcemaps.init())
		.pipe(postcss([csswring({removeAllComments: true, preserveHacks: true})]))
		
		.pipe(rename(function (path) { path.extname = ".min.css"; }))
		.pipe(sourcemaps.write('./maps'))
	 	.pipe(gulp.dest(assets_folder+'/styles'));
});

gulp.task('scripts', function() {
	return gulp.src(assets_folder+'/scripts.src/*.js')
		.pipe(sourcemaps.init())
		.pipe(uglify())
		.pipe(rename(function(path) { path.extname = ".min.js"; }))
		.pipe(sourcemaps.write('./maps'))
		.pipe(gulp.dest(assets_folder+'/scripts'));
});

gulp.task('watch', function() {
	fatalLevel = 'off';
	gulp.watch(assets_folder+'/scripts.src/*', ['lint', 'scripts']);
	gulp.watch(assets_folder+'/styles.src/*', ['styles']);
});

gulp.task('buildonce', ['styles', 'lint', 'scripts']);
gulp.task('default', ['buildonce', 'watch']);