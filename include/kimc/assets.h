/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */


#ifndef _KCALEM_ASSETS_H
# define _KCALEM_ASSETS_H	1

const char
	*KCD_Game,
	*KCF_Game_PlayersNum,
	*KCF_Game_Day,
	*KCF_Game_State,
	*KCF_Game_Timer,
	*KCF_Game_Roles,
	*KCF_Game_Chat,
	*KCF_Game_Judging,
	*KCD_Player,
	*KCF_Player_Password,
	*KCF_Player_State,
	*KCF_Player_Nick,
	*KCF_Player_Killers,
	*KCF_Player_LastWill,
	*KCF_Player_DeathNote,
	*KCF_Player_Vote,
	*KCF_Player_Judgm,
	*KCF_Player_Night,
	*KCF_Player_NightResult,
	*KCD_Role,
	*KCF_Role_Name,
	*KCF_Role_Desc,
	*KCF_Role_Pros,
	*KCF_Role_Cons,
	*KCF_Role_Obj;

const char
	*KCF_PgSnippets_BootstrapHeader,
	*KCF_PgSnippets_EnterNick,
	*KCF_PgSnippets_GameBase;

#endif