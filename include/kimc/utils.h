/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

#ifndef _KCALEM_UTILS_H
#define _KCALEM_UTILS_H	1

#include <stdbool.h>
#include <stdio.h>

const char *KC_F_W, *KC_F_R, *KC_F_A;
KCRoleData *KC_ROLES_CACHE[_RL_NUM];

void mkdir_p(char *dir);
void urldecode2(char *dst, const char *src);

KCGame *kcgame_read(KCGameId id, KCGame *dest);
bool kcgame_write(KCGame *game);
KCPlayerId kcgame_slots_pop(KCGame *game);
KCPlayer *kcgame_players_load(KCGame *game);
long int kcgame_chat_size(KCGameId game_id);
FILE *kcgame_chat_open(KCGameId game_id);
bool kcgame_state_update(KCGame *game);

KCPlayerId kcgame_judging_get(KCGame *game, unsigned char day);
KCJudgResult kcgame_judging_isguilty(KCGame *game, unsigned char day);
void kcgame_killplayer(KCGameId game_id, KCPlayerId player_id);
KCPlayerId kcplayer_action_target(KCPlayer *player, unsigned char day);

bool kcplayer_write(KCPlayer *player);
bool kcplayer_nick_write(KCPlayer *player);
bool kcplayer_validate(KCPlayer *player);
KCPlayer *kcplayer_read(KCPlayer *player);
bool kcplayer_state_update(KCPlayer *player);

void kcroles_rand(size_t rn, KCRoles *r);
KCRoleData *kcroles_data_get(KCRoles id);

void kc_fixpath();
void kc_cat(char *fname, FILE *out);
bool kc_fcat(char *fname, FILE *stream, ...);

#endif