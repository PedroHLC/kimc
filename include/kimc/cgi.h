#ifndef _KCALEM_CGI_H
# define _KCALEM_CGI_H	1

#include <stdio.h>

void kccgi_html_begin();
int kccgi_diebyparams();
int kccgi_diebyinvalidpass();

typedef struct {
	char *key;
	char *val;
} KCXMLParam;

void kcxml_tparam_ins(size_t params_num, KCXMLParam *params, FILE *stream);
void kcxml_tag_open(char *tag, size_t params_num, KCXMLParam *params, FILE *stream);
void kcxml_tag_close(char *tag, FILE *stream);
void kcxml_tag_ins(char *tag, size_t params_num, KCXMLParam *params, FILE *stream);
void kcxml_tag_insc(char *tag, size_t params_num, KCXMLParam *params, char *contents, FILE *stream);
void kcxml_css_ins(char *url, FILE *stream);

const char *XML_HTML,
	*XML_HTML_LANG,
	*HTML_HEAD,
	*HTML_META,
	*HTML_TITLE,
	*HTML_LINK,
	*HTML_LINK_REL,
	*HTML_LINK_TYPE,
	*HTML_LINK_HREF,
	*HTML_SCRIPT,
	*HTML_BODY,
	*HTML_DIV,
	*HTML_SPAN,
	*HTML_BR,
	*HTML__CLASS;

const char *LANG_PTBR,
	*CHAT_CLASS_SERVER,
	*CHAT_CLASS_PLAYER;

const char *HTML_TAG,
	*HTML_TAGPARAM;

#define CGI_PLAINTEXT "Content-type: text/plain; charset=UTF-8\r\n"

#define KCPageBegin() \
	kccgi_html_begin(); \
	kcxml_tag_open(XML_HTML, 1, (KCXMLParam[]){ \
		(KCXMLParam){XML_HTML_LANG, LANG_PTBR} \
	}, stdout); \
	kcxml_tag_open(HTML_HEAD, 0, NULL, stdout);

#define KCPageTitle(x) \
	kcxml_tag_insc(HTML_TITLE, 0, NULL, x, stdout);

#define KCPageGoBody() \
	kcxml_tag_close(HTML_HEAD, stdout); \
	kcxml_tag_open(HTML_BODY, 0, NULL, stdout);

#define KCPageEnd() \
	kcxml_tag_close(HTML_BODY, stdout); \
	kcxml_tag_close(XML_HTML, stdout);

#define KCChatAnnounce(stream, format, ...) \
	kcxml_tag_open(HTML_SPAN, 1, (KCXMLParam[]){ \
		(KCXMLParam){HTML__CLASS, CHAT_CLASS_SERVER} \
	}, stream); \
	fprintf(stream, format, ## __VA_ARGS__); \
	kcxml_tag_close(HTML_SPAN, stream); \
	kcxml_tag_ins(HTML_BR, 0, NULL, stream);
	

#endif