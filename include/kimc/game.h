/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */


#ifndef _KCALEM_H
# define _KCALEM_H	1

#include <stdbool.h>
#include <limits.h>
#include <time.h>
#include <kimc/assets.h>

#define KC_PLAYERID_MAX UCHAR_MAX
typedef unsigned char KCPlayerId;
#define KC_PLAYERID_INVALID 255
#define KC_GAMEID_MAX UINT_MAX
typedef unsigned int KCGameId;

typedef enum {
	_RL_NDF=0,
	
	RL_SHERIFF=1,
	RL_INVESTIGATOR=2,
	RL_MEDIC=3,
	RL_MEDIUM=4,
	RL_LOOKOUT=5,
	RL_JAILOR=6,
	RL_VETERAN=7,
	RL_EXECUTIONER=8,
	RL_VIGILANT=9,

	RL_JESTER=10,
	RL_FRAMER=11,

	RL_MAFIOSO=12,

	RL_SERIALKILLER=13,

	RL_DUMMYTOWNER=14,
	RL_DETECTIVE=15
} KCRoles;

typedef struct {
	KCPlayerId num;
	KCRoles *roles;
} KCGameMode;

#define _RL_NUM 15

typedef enum {
	ST_ALIVE=0,
	ST_DEAD,
	ST_CONNECTING,
	ST_DISCONNECTED,
} KCPlayerState;

typedef struct {
	KCGameId game;
	KCPlayerId id;
	unsigned int password;
} KCPlayerPass;

typedef struct {
	//KCPlayerPass
		KCGameId game;
		KCPlayerId id;
		unsigned int password;
	char nick[20];
	KCPlayerState state;
	char *last_will, *death_note;
	size_t killers_n;
	KCPlayerId *killers;
} KCPlayer;

typedef enum {
	DS_WAITINGPLAYERS=9,
	DS_DAY=1,
	DS_NIGHT=2,
	DS_JUDGMENT=3,
	DS_ENDED_DRAW=10,
	DS_ENDED_TOWNERS=11,
	DS_ENDED_MAFIA=12,
	DS_ENDED_SK=13
} KCGameState;

typedef struct {
	KCGameId id;
	KCPlayerId p_num;
	unsigned char day;
	KCGameState state;
	time_t timer;
	KCPlayerId slots_max;
	KCRoles *roles;
	KCPlayerId judging;
} KCGame;

typedef struct {
	KCRoles id;
	char name[20];
	char *desc, *pros, *cons, *obj;
} KCRoleData;

typedef enum {
	VT_NONE=0,
	VT_GUILTY=1,
	VT_INNOCENT=2,
	VT_DRAW=3
} KCJudgResult;

#define KC_FPATH_LIMIT PATH_MAX+1

#endif