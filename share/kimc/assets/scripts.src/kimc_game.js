/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  Copyright (C) 2016 Pedro Henrique Lara Campos
 */

Date.prototype.getUnixTime = function() { return this.getTime()/1000|0; };

var game={
	'players':[],
	'loaded_chat':0,
	'state':9,
	'timer':0,
	'judging':0,
	'day':0
};
var chat='';
var game_xhttp = new XMLHttpRequest();
var chat_xhttp = new XMLHttpRequest();
var chatsend_xhttp = new XMLHttpRequest();
var playerdoact_xhttp = new XMLHttpRequest();
var plr_menu_dom = document.getElementById("plr_menu");
var chat_log_dom = document.getElementById("chat_log");
var chat_in_dom = document.getElementById("chat_in");
var gm_day_dom = document.getElementById("gm_day");
var gm_state_dom = document.getElementById("gm_state");
var gm_timer_dom = document.getElementById("gm_timer");
var popup_dom = document.getElementById("popup");
function updateChat(off, end) {
	chat_xhttp.open('GET', 'chat.cgi?game='+me.game+"&off="+off+"&end="+end, true);
	chat_xhttp.send();
}
function updateGame(new_game) {
	var me_plr = new_game.players[me.id];
	var i;
	if(game.loaded_chat < new_game.loaded_chat) {
		updateChat(game.loaded_chat, new_game.loaded_chat);
	}
	if(game.players.length < new_game.players.length)
		for(i=game.players.length; i < new_game.players.length; i++) {
			plr_menu_dom.innerHTML += '<div class="row" id="mn_plr'+i+'"><div class="col-xs-9"><span id="icons_plr'+i+'"><span class="veico" aria-hidden="true">J&nbsp;</span></span><span id="name_plr'+i+'">'+new_game.players[i].nick+'</span></div><div class="col-xs-3 text-right"><button id="doacton_plr'+i+'" class="btn btn-default" onclick="playerDoAction('+i+')" disabled>AGIR</button></div></div>';
		}
	for(i=0; i < game.players.length; i++) {
		var owrk = game.players[i];
		var nwrk = new_game.players[i];
		if(owrk.nick != nwrk.nick) {
			document.getElementById("name_plr"+i).innerHTML = nwrk.nick;
		}
		document.getElementById("icons_plr"+i).firstChild.style.display = (nwrk.state === 0 ? 'none' : false);
	}
	//TODO


	if(game.state != new_game.state) {
		popup_dom.innerHTML = '';
		var txt;
		switch(new_game.state) {
			case 9: txt="Esperando Jogadores..."; break;
			case 1: txt="Turno: Dia";
				new_game.players.forEach(function(pl, pl_id) {
					var btn = document.getElementById("doacton_plr"+pl_id);
					btn.disabled = (me_plr.state !== 0 || pl.state !== 0 || game.day < 1);
					btn.innerHTML = "Votar";
				});
				break;
			case 2: txt="Turno: Noite";
				new_game.players.forEach(function(pl, pl_id) {
					var btn = document.getElementById("doacton_plr"+pl_id);
					btn.disabled = (me_plr.state !== 0 || pl.state !== 0 || me_plr.role == 14 || pl.role == 12 || pl_id == me.id);
					btn.innerHTML = "Visitar";
				});
				break;
			case 3: txt="Turno: Julgamento";
				new_game.players.forEach(function(pl, pl_id) {
					var btn = document.getElementById("doacton_plr"+pl_id);
					btn.disabled = true;
					btn.innerHTML = "-";
				});
				if(new_game.judging !== 0 && me_plr.state === 0) {
					popup_dom.innerHTML += '<h2>A cidade vota contra '+new_game.players[new_game.judging].nick+'</h2>Decida seu destino:<br/><div class="row">'+
						'<div class="col-sm-6 text-center"><button class="btn btn-danger" onclick="playerDoAction(1);">CULPADO</button></div>'+
						'<div class="col-sm-6 text-center"><button class="btn btn-info" onclick="playerDoAction(2);">INOCENTE</button></div>'+
						'</div>';
				}
				break;
			case 11: txt="Jogo acabou! (Vitória dos cidadãos)"; break;
			case 13: txt="Jogo acabou! (Vitória do assassino)"; break;
			//default: txt="Jogo acabou!"; break;
		}
		gm_state_dom.innerHTML = txt;
	}
	if(new_game.timer === 0) {
		if(game.timer !== 0)
			gm_timer_dom.innerHTML = '<strong>&infin;</strong>';
	} else
		gm_timer_dom.innerHTML = '<strong>'+(new_game.timer - (new Date()).getUnixTime())+'s</strong>';
	if(game.day != new_game.day)
		gm_day_dom.innerHTML = 'Dia: '+new_game.day;

	if(new_game.state == 11 || new_game.state == 13)
		popup_dom.innerHTML = '<h1>Obrigado por jogar!</h1>';
	else if(new_game.players[me.id].state == 1)
		popup_dom.innerHTML = '<h1>Você está morto</h1>';
	game = new_game;
}
function stepGame() {
	game_xhttp.open('GET', 'status.cgi'+window.location.search, true);
	game_xhttp.send();
}
game_xhttp.onreadystatechange = function() {
	if (game_xhttp.readyState == 4) {
		if(game_xhttp.status == 200)
			updateGame(JSON.parse(game_xhttp.responseText));
		setTimeout(stepGame, 500);
	}
};
chat_xhttp.onreadystatechange = function() {
	if (chat_xhttp.readyState == 4 && chat_xhttp.status == 200) {
		chat_log_dom.innerHTML += chat_xhttp.responseText;
		chat_log_dom.scrollTop = chat_log_dom.clientHeight;
	}
};
stepGame();
function sendChat() {
	chatsend_xhttp.open('GET', 'chat_send.cgi' + window.location.search + '&msg=' + chat_in_dom.value, true);
	chatsend_xhttp.send();
	chat_in_dom.value = '';
}
function playerDoAction(target) {
	playerdoact_xhttp.open('GET', 'player_doact.cgi' + window.location.search+'&target='+target, true);
	playerdoact_xhttp.send();
}